package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;

@Component
public class VideoGamesService {

    private VideoGamesRepository videoGamesRepository;
    private RawgDatabaseClient rawgDatabaseClient;

    @Autowired
    public VideoGamesService(VideoGamesRepository videoGamesRepository, RawgDatabaseClient rawgDatabaseClient) {
        this.videoGamesRepository = videoGamesRepository;
        this.rawgDatabaseClient = rawgDatabaseClient;
    }

    public List<VideoGame> ownedVideoGames() {
        return this.videoGamesRepository.getAll();
    }

    public Optional<VideoGame> getOneVideoGame(Long id) {
        return this.videoGamesRepository.get(id);
    }

    public Optional<VideoGame> addVideoGame(String name) {

        Optional<VideoGame> optionalVideoGame = Optional.ofNullable(rawgDatabaseClient.getVideoGameFromName(name));
        optionalVideoGame.ifPresent(x -> videoGamesRepository.save(x));

        return optionalVideoGame;
    }

    public void deleteAVideoGameInRepository(Long id){
        this.videoGamesRepository.deleteVideoGame(id);
    }
}
