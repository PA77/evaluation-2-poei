package com.zenika.academy.videogames.controllers;


import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.VideoGamesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class VideoGameGenerator {

    private VideoGamesService videoGamesService;
    private VideoGamesRepository videoGamesRepository;

    @Autowired
    public VideoGameGenerator(VideoGamesService videoGamesService){
        this.videoGamesService = videoGamesService;
    }

    @PostConstruct
    public void generate(){
        videoGamesService.addVideoGame("The Last Of Us");
        videoGamesService.addVideoGame("Final Fantasy 7");
        videoGamesService.deleteAVideoGameInRepository(1L);
    }
}
