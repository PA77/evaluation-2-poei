package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class VideoGamesRepositoryTest {

    private VideoGamesRepository videoGamesRepository;
    private VideoGame videoGame;
    private VideoGame videoGame2;


    @BeforeEach
    void setUp(){
        videoGamesRepository = new VideoGamesRepository();
        Genre genre = new Genre("Action");
        Genre genre1 = new Genre("Aventure");
        videoGame = new VideoGame(1L,"The Last Of Us", List.of(genre, genre1));
        videoGame2 = new VideoGame(2L, "The Witcher", List.of(genre, genre1));
    }

    @Test
    void getAll() {
        this.videoGamesRepository.save(videoGame);
        this.videoGamesRepository.save(videoGame2);
        assertEquals(2L, this.videoGamesRepository.getAll().size());
        assertEquals("The Last Of Us", this.videoGamesRepository.getAll().get(0).getName());
        assertEquals("The Witcher", this.videoGamesRepository.getAll().get(1).getName());

    }

    @Test
    void get() {
        this.videoGamesRepository.save(videoGame);
        assertEquals("The Last Of Us", this.videoGamesRepository.get(1L).get().getName());
        assertEquals(2 ,this.videoGamesRepository.get(1L).get().getGenres().size());
    }

    @Test
    void save() {
        this.videoGamesRepository.save(videoGame);
        assertEquals(1, this.videoGamesRepository.getAll().size());
    }

    @Test
    void delete(){
        this.videoGamesRepository.save(videoGame);
        this.videoGamesRepository.save(videoGame2);
        this.videoGamesRepository.deleteVideoGame(1L);
        assertEquals(1, this.videoGamesRepository.getAll().size());
    }
}