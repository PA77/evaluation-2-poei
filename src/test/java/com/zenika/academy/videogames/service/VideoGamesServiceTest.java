package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class VideoGamesServiceTest {

    private VideoGamesRepository videoGamesRepository;
    private VideoGamesService videoGamesService;


    @BeforeEach
    void setUp(){
        videoGamesRepository = new VideoGamesRepository();
        RawgDatabaseClient rawgDatabaseClient = new RawgDatabaseClient();

        videoGamesService = new VideoGamesService(videoGamesRepository, rawgDatabaseClient);

    }

    @Test
    void ownedVideoGames() {
        Genre genre = new Genre("Action");
        Genre genre1 = new Genre("Aventure");
        VideoGame videoGame = new VideoGame(1L,"Bye Bye Deadman", List.of(genre, genre1));
        VideoGame videoGame1 = new VideoGame(2L,"Toto", List.of(genre1, genre));
        videoGamesRepository.save(videoGame);
        videoGamesRepository.save(videoGame1);

        assertEquals(2, videoGamesService.ownedVideoGames().size());


    }

    @Test
    void getOneVideoGame() {
        Genre genre = new Genre("Action");
        Genre genre1 = new Genre("Aventure");
        VideoGame videoGame = new VideoGame(1L,"Bye Bye Deadman", List.of(genre, genre1));
        VideoGame videoGame1 = new VideoGame(2L,"Toto", List.of(genre1, genre));
        videoGamesRepository.save(videoGame);
        assertEquals("Bye Bye Deadman", videoGamesService.getOneVideoGame(1L).get().getName());
    }

    @Test
    void addVideoGame() {
        Genre genre = new Genre("Action");
        Genre genre1 = new Genre("Aventure");
        VideoGame videoGame = new VideoGame(1L,"Bye Bye Deadman", List.of(genre, genre1));
        RawgDatabaseClient mock = mock(RawgDatabaseClient.class);
        when(mock.getVideoGameFromName("Bye Bye Deadman")).thenReturn(videoGame);

        this.videoGamesService.addVideoGame("Bye Bye Deadman");;
        assertEquals(0, videoGamesRepository.getAll().size());
    }
}